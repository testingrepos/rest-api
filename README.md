## RestAPI test automation framework

This is a bootstrap project for setting up REST-API automated tests.

#### Example RestAPI
You can find `.jar` file with example API to run your test against: `complete-rest-0.1.0.jar`. 
To run it locally, execute: `java -jar complete-rest-0.1.0.jar` in your terminal (Java 8 and `JAVA_HOME` set required).
Application will run at http://127.0.0.1:8080/.


#### Documentation
Once the application will be running, you can check interactive **OpenAPI documentation** for exposed RestAPI at: http://127.0.0.1:8080/swagger-ui.html 
